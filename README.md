Linki do artykułów (10.04.22):
<ul>
    <li>https://www.baeldung.com/spring-thymeleaf-fragments</li>
    <li>https://www.baeldung.com/spring-requestparam-vs-pathvariable</li>
</ul>
<hr>
Linki do artykułów (23.04.22):
<ul>
    <li>https://www.baeldung.com/thymeleaf-iteration</li>
    <li>https://www.baeldung.com/dates-in-thymeleaf</li>
</ul>
<hr>
Zadanie 1:
<ul>
    <li>Na wzór zakładki blog - utwórz zakładkę "produkty"</li>
    <li>Kod będzie znajdował się w ProductController (url to "/products")</li>
    <li>Utwórz klasę Product z polami (id, nazwa, opis, cena, utworzono, zaktualizowano (daty))</li>
    <li>Na wzór zakładki blog - wypełnij listę produktów przykładowymi danymi</li>
    <li>Zakładka "produkty" powinna być osiągalna z menu głównego - pokazywać listę produktów z "bazy"</li>
    <li>Na zwór parametru "m" dla blog - utwórz możliwość filtrowania po cenie (parametr "p")</li>
    <li>*cena +/- 20%</li>
</ul>
<hr>
Zadanie 2:
<ul>
    <li>Zmodyfikować zadanie 1 aby filtrować po cenie od-do (parametr "pf" i "pt")</li>
    <li>Wyszukiwanie po nazwie (parametr "n"). Funkcja contains na stringu</li>
    <li>Utworzyć enum z kategorią produktu (telefony, laptopy, tv, monitory)</li>
    <li>Dodać do klasy Produkt pole z enumem z kategorią produktu i umożliwić wyszukiwanie po parametrze "c" np. c=phones</li>
</ul>
<hr>
Zadanie 3:
<ul>
    <li>Na wzór formularza w zakładce blog dodać formularz filtrujący w zakładce produkty</li>
    <li>Fitrujemy po "pf" i "pt" (input number) oraz "n" i "c" (input text)</li>
    <li>*parametr "c" powinien działać jako select z listą rozwijaną renderowaną z enuma</li>
</ul>
<hr>
Zadanie 4:
<ul>
    <li>Na zwór formularza dodawania produktu utworzyć formularz dodawania artykułu (blog)</li>
    <li>Formularz wysyłamy metodą POST na @PostMapping - odbieramy dane jako Article</li>
    <li>Dodajemy do listy articles i przekierujemy na zakładkę blog</li>
</ul>
<hr>
Linki do artykułów (24.04.22):
<ul>
    <li>https://www.baeldung.com/thymeleaf-select-option</li>
</ul>
<hr>
Zadanie 5:
<ul>
    <li>Utworzyć enum ArticleCategory (TECHNICAL, GUIDE, OTHER)</li>
    <li>Do klasy Article dodać pole category o typie ArticleCategory</li>
    <li>Wygenerować od nowa gettery i settery, kontruktor dla Article (nalezy uzupełnić listę w ArticleController o nowe pole)</li>
    <li>Do zakładki blog dodać możliwość filtrowania po kategorii (enum ArticleCategory, na wzór kategorii w produktach)</li>
    <li>Do formularza dodawania nowego artykułu dodać możliwość wybrania kategorii z listy (wzór produkty)</li>
    <li>Formularz dodawania nowego artykułu zapisuje się wraz z kategorią</li>
</ul>
<hr>
Zadanie 6:
<ul>
    <li>Wyświelić na liście artykuów - kategorię artykułu</li>
    <li>Wyświelić na liście produktów - kategorię produktów</li>
    <li>Do produktu dodać pole obrazek (String image z adresem url do obrazka znalezionego w google)</li>
    <li>Wyświetlić obrazek w zakładce products (th:src)</li>
</ul>
<hr>
Zadanie domowe (na 07.05.22):
<ul>
    <li>Utworzyć enum ServiceCategory (REPAIR, ADVICE)</li>
    <li>Utworzyć klasę Service (pola: id, name, content, price, category (typ ServiceCategory), createdAt, updatedAt) na wzór Product</li>
    <li>Wygenerować getery i setery, konsruktor do klasy Service</li>
    <li>Utworzyć formularz pozwalający na dodanie usługi (pola: name, content, price, category)</li>
    <li>Utworzyć ServiceController będący kontrolerem umożliwiającym pokazanie listy usług oraz formularza dodawania usługi (/services i /add-service)</li>
    <li>Dodać w nawigacji możliwość uruchomienia strony "/services" i "/add-service"</li>
    <li>W ServiceController istnieje lista usług wypełniona na wzór ProductController</li>
    <li>Na widoku listy usług dodajemy możliwość filtrowania po kategorii oraz wyszukiwania po nazwie usługi</li>
    <li>Istnieje możliwość wysłania formularza "dodawania nowej usługi" - po dodaniu do listy redirect na listę usług</li>
</ul>
<hr>
Zadanie 7:
<ul>
    <li>Na wzór usuwania artykułu zaimplementować usuwanie produktu</li>
</ul>
<hr>
Zadanie 8:
<ul>
    <li>Na wzór edycji artykułu zaimplementować edycję produktu</li>
</ul>
<hr>
Zadanie 9:
<ul>
    <li>Dla artykułu dodać stronę "szczegóły" per konkretny art, która wyświetla wszystkie dane konkretnego artykułu, a na liście wszystkich znajduje się jedynie tytuł i kategoria</li>
    <li>Dla produktu dodać stronę "szczegóły" per konkretny prod, która wyświetla wszystkie dane konkretnego produktu, a na liście wszystkich znajduje się jedynie nazwa, kategoria i cena</li>
</ul>
<hr>
Zadanie 10 - na wzór listy produktów, podglądu produktu i edycji produktu zastosować klasy bootstrap do:
<ul>
    <li>Listy artykułów</li>
    <li>Podglądu artykułu</li>
    <li>Edycji artykułu</li>
</ul>
<hr>
Linki 08.05.22:
<ul>
    <li>https://getbootstrap.com/docs/5.1/getting-started/introduction/</li>
    <li>https://icons.getbootstrap.com/</li>
    <li>https://www.samouczekprogramisty.pl/solid-czyli-dobre-praktyki-w-programowaniu-obiektowym/</li>
    <li>https://spring.io/guides/gs/accessing-data-jpa/</li>
    <li>https://www.baeldung.com/spring-boot-h2-database</li>
    <li>https://www.baeldung.com/jpa-persisting-enums-in-jpa</li>
</ul>
<hr>
Zadanie 11:
<ul>
    <li>Na wzór klasy ArticleService utworzyć ProductService</li>
    <li>Przenieść wszystkie funkcje do Serwisu (lista, uzupełnienie listy, dodawanie, usuwanie)</li>
</ul>
<hr>
Zadanie domowe (na 21.05.22):
<ul>
    <li>Utworzyć ProductRepository</li>
    <li>Utworzyć ProductDatabaseService</li>
    <li>Wstrzyknąć ProductRepository do ProductDatabaseService</li>
    <li>ProductDatabaseService powinien być adnotowany jako serwis aby mógł być "wstrzykiwany"</li>
    <li>Zmienić nazwę klasy ProductService na ProductListService</li>
    <li>Na podstawie klasy ProductListService utworzyć interface ProductService (extract interface z InteliJ)</li>
    <li>ProductDatabaseService powinien implementować interface ProductService</li>
    <li>ProductListService powinien implementować interface ProductService</li>
    <li>Wstrzyknąć ProductService do ProductController</li>
    <li>Na wzór ArticleDatabaseService napisać implementację metod dla ProductDatabaseService</li>
    <li>ProductDatabaseService i ProductListService powinny mieć adnotację serwis aby można było je "wstrzykiwać"</li>
    <li>ProductDatabaseService powinien być domyślnym serwisem (w innym razie dojdzie do konfliktu w kontrolerze)</li>
    <li>uruchomić projekt dodać produkt i zobaczyć czy pojawi się w bazie danych</li>
</ul>
Uwagi do zadania domowego:
<ul>
    <li>baza danych osiągalna z adresu url: http://localhost:8080/h2-console</li>
    <li>JDBC URL: jdbc:h2:mem:db</li>
    <li>User Name: sa</li>
    <li>Password: sa</li>
</ul>
<hr>
Linki do artykułów (21.05.22):
<ul>
    <li>https://www.baeldung.com/spring-autowire</li>
    <li>https://www.baeldung.com/h2-embedded-db-data-storage</li>
    <li>https://projectlombok.org/</li>
</ul>
<hr>
Zadanie 12:
<p>Na wzór ArticleDatabaseService i wyszukiwaniu po parametrach getArticlesByParams wykonać metodę, która:</p>
<ul>
    <li>pozwoli na filtrowanie po cenie minimalnej</li>
    <li>pozwoli na filtrowanie po cenie maksymalnej</li>
    <li>pozwoli na filtrowanie po nazwie (like %%)</li>
    <li>pozwoli na filtrowanie po kategorii</li>
</ul>
<p>*zapytanie musi być w stylu (:cat is null or a.category = :cat) aby umożliwiać filtrowanie selektywne po przekazanych parametrach - omijając parametry nieprzekazane</p>
<hr>
Zadanie 13:
<ul>
    <li>na wzór Article dodać do Product adnotacje lombok getter/setter i konstruktory</li>
    <li>na wzór ProductController dodać konstruktor do ArticleController dla pól finalnych</li>
</ul>
<hr>
Zadanie 14:
<p>Dodać kontruktor pól finalnych poprzez lombok dla:</p>
<ul>
    <li>ArticleDatabaseService</li>
    <li>ProductDatabaseService</li>
</ul>
<hr>
Linki do artykułów (22.05.22):
<ul>
    <li>https://howtodoinjava.com/design-patterns/creational/builder-pattern-in-java/</li>
    <li>https://howtodoinjava.com/lombok/lombok-builder-annotation/</li>
    <li>https://www.baeldung.com/spring-boot-bean-validation</li>
    <li>https://www.baeldung.com/spring-thymeleaf-error-messages</li>
</ul>
<hr>
Zadanie 15:
<ul>
    <li>Na podstawie Article dodać walidację do Product</li>
    <li>Sprawdzić czy walidacje w Product są zgodne z saveProduct.html</li>
</ul>
<hr>
Zadanie 16:
<p>Na podstawie ArticleController -> addArticle i editArticle</p>
<ul>
    <li>Dodać walidację dla ProductController (metod addProduct i editProduct)</li>
    <li>Odebrać wyniki walidacji z BindingResult</li>
    <li>Pobrać pierwszy błąd i wyświetlić go na konsoli</li>
    <li>Przekazać błąd do saveProduct.html</li>
    <li>Wyświetlić błąd w saveProduct.html</li>
</ul>
<hr>
Zadanie domowe (na 04.06.22):
<p>Na podstawie ArticleRestController</p>
<ul>
    <li>Utworzyć w katalogu rest -> ProductRestController</li>
    <li>ProductRestController będzie adnotowany @RestController</li>
    <li>Będzie posiadał metody GET (pobranie listy oraz pojedyńczego elementu), POST, PUT i DELETE pozwalacjące na zapis, aktualizację, usuwanie i pobieranie danych</li>
    <li>Sprawdzamy nowo utworzone endpointy postmanem (https://www.postman.com/)</li>
</ul>
<hr>
Linki do artykułów (04.06.22):
<ul>
    <li>https://www.baeldung.com/exception-handling-for-rest-with-spring</li>
    <li>https://stackoverflow.com/questions/47230057/adding-a-custom-message-to-a-exceptionhandler-in-spring/47231271#47231271</li>
    <li>https://www.baeldung.com/java-record-keyword</li>
    <li>https://www.baeldung.com/spring-security-login</li>
    <li>https://spring.io/guides/gs/securing-web/</li>
    <li>https://www.baeldung.com/spring-security-method-security</li>
    <li>https://www.baeldung.com/spring-security-5-default-password-encoder</li>
    <li>https://github.com/thymeleaf/thymeleaf-extras-springsecurity</li>
    <li>https://www.baeldung.com/security-spring</li>
</ul>
<hr>
Zadanie 17:
<ul>
    <li>Na podstawie products.html zabezpiecz articles.html (przyciski)</li>
    <li>Na podstawie ProductController zabezpiecz ArticleController</li>
    <li>Zaloguj się jako admin, manager i author i sprawdź uprawnienia do endpointów i widoczność przycisków</li>
</ul>
<hr>
Linki do artykułów (05.06.22):
<ul>
    <li>https://www.baeldung.com/spring-security-remember-me</li>
    <li>https://www.javadevjournal.com/spring/spring-security-userdetailsservice/</li>
    <li>https://jessitron.com/2020/06/15/spring-security-for-h2-console/</li>
    <li>https://www.baeldung.com/spring-security-authentication-with-a-database</li>
    <li>https://sekurak.pl/czym-jest-podatnosc-csrf-cross-site-request-forgery/</li>
    <li>https://www.baeldung.com/jpa-one-to-one</li>
    <li>https://www.baeldung.com/hibernate-one-to-many</li>
    <li>https://www.baeldung.com/jpa-many-to-many</li>
</ul>
<hr>
<ul>
    <li>admin / TrudneHasłoAdmina ($2a$10$cC3q4HLOIYRufa0z/32hCufwW83diTEjdaVzNDQLChbzcJgbT0lP6)</li>
    <li>manager / TrudneHasłoManagera ($2a$10$jT9JaJpNwC/3c39UPjP.B.e3ZVCaIAvY73cn940gdxuOePZ6I6wPW)</li>
    <li>author / TrudneHasłoAutora ($2a$10$1vDg0lpWx8hCAzPkiVl0menFwcTZxzro7zKGN20G4YOwsWadiFJIC)</li>
</ul>
<hr>
Zadanie domowe (na 02.07.22):
<ul>
    <li>Na podstawie ArticleDatabaseServiceTest napisać testy dla ProductDatabaseService (ProductDatabaseServiceTest)</li>
    <li>Na podstawie ArticleListServiceTest napisać testy dla ProductListService (ProductListServiceTest)</li>
</ul>
<hr>
Linki do artykułów (02.07.22):
<ul>
    <li>https://www.baeldung.com/spring-security-integration-tests</li>
    <li>https://www.baeldung.com/integration-testing-in-spring</li>
</ul>
<hr>
Zadanie domowe BACK:
<ul>
    <li>Na podstawie ArticleRestControllerTest napisać testy integracyjne dla ProductRestController</li>
</ul>
<hr>
Zadanie domowe FRONT:
<ul>
    <li>Na podstawie zakładki Articles utworzyć zakładkę Products</li>
    <li>Dodać routing dla Products (komponenty Products i Product)</li>
    <li>ProductsComponent to komponent z listą produktów</li>
    <li>ProductComponent to komponent widokiem pojedynczego produktu</li>
    <li>ProductComponent ma przyciski usuwania i edycji</li>
    <li>Utworzyć ProductService, który pozwala na odpytywanie backendu (dodanie produktu, edycja, usuwanie i pobieranie)</li>
    <li>Utworzyć formularz dodawania / edycji produktu na wzór ArticleFormComponent</li>
    <li>Dodać do formularza odpowiednią walidację</li>
    <li>Ostylować za pomocą klas boostrapa aplikację aby nikt nie dostał zawału serca oglądając ją :)</li>
    <li>ps. w razie pytań o treść zadania lub pochwalenia się wykonaniem - piszcie! Będzie mi bardzo miło! :)</li>
</ul>
<hr>
