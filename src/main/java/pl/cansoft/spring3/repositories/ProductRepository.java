package pl.cansoft.spring3.repositories;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.cansoft.spring3.models.Product;
import pl.cansoft.spring3.models.ProductCategory;

public interface ProductRepository extends JpaRepository<Product, Long> {

    @Query("""
        select p from Product p where
        (:name is null or p.name like %:name%) and
        (:category is null or p.category = :category) and
        (:priceFrom is null or p.price >= :priceFrom) and
        (:priceTo is null or p.price <= :priceTo)
        """)
    List<Product> findByParams(
        @Param("priceFrom") Integer priceFrom,
        @Param("priceTo") Integer priceTo,
        @Param("name") String name,
        @Param("category") ProductCategory category
    );
}
