package pl.cansoft.spring3.repositories;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import pl.cansoft.spring3.models.Article;
import pl.cansoft.spring3.models.ArticleCategory;

public interface ArticleRepository extends JpaRepository<Article, Long> {

    @Query("""
        select a from Article a
        where a.title like %:title%
        """)
    List<Article> findByTitle(@Param("title") String title);

    @Query("""
        select a from Article a
        where (:cat is null or a.category = :cat) and
        ((:fromDate is null and :toDate is null) or a.createdAt between :fromDate and :toDate)
        """)
    List<Article> findByParams(
        @Param("cat") ArticleCategory category,
        @Param("fromDate") LocalDateTime fromDate,
        @Param("toDate") LocalDateTime toDate
    );
}
