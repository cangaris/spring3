package pl.cansoft.spring3.repositories;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import pl.cansoft.spring3.models.User;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);
}
