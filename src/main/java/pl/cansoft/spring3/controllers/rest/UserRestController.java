package pl.cansoft.spring3.controllers.rest;

import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.spring3.models.User;
import pl.cansoft.spring3.repositories.UserRepository;

@RequiredArgsConstructor
@RestController
@RequestMapping("rest/users")
class UserRestController {

    private final UserRepository repository;

    @GetMapping
    public List<User> getUsers() {
        return repository.findAll();
    }
}
