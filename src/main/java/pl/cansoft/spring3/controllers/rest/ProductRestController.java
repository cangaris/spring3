package pl.cansoft.spring3.controllers.rest;

import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.spring3.models.Product;
import pl.cansoft.spring3.models.ProductCategory;
import pl.cansoft.spring3.services.products.ProductService;

/**
 * CRUD
 * C - create (POST)
 * R - read (GET get list, get one by id)
 * U - update (PUT)
 * D - delete (DELETE)
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("rest/products")
class ProductRestController {

    private final ProductService productService;

    @GetMapping
    public List<Product> getProducts(@RequestParam(required = false, name = "pf") Integer priceFrom,
                                     @RequestParam(required = false, name = "pt") Integer priceTo,
                                     @RequestParam(required = false, name = "n") String name,
                                     @RequestParam(required = false, name = "c") ProductCategory category) {
        return productService.getProductsByParams(priceFrom, priceTo, name, category);
    }

    @GetMapping("{id}")
    public ResponseEntity<Product> getProduct(@PathVariable Long id) {
        var optionalProduct = productService.findProduct(id);
        return optionalProduct
            .map(ResponseEntity::ok) // product -> ResponseEntity.ok(product)
            .orElseGet(() -> ResponseEntity.notFound().build());
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER", "ROLE_AUTHOR"})
    @PostMapping
    public ResponseEntity<Product> createProduct(@RequestBody @Valid Product product) {
        return ResponseEntity.ok(productService.addProduct(product));
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER"})
    @PutMapping("{id}")
    public ResponseEntity<Product> updateProduct(@PathVariable Long id, @RequestBody @Valid Product product) {
        return ResponseEntity.ok(productService.updateProduct(id, product));
    }

    @Secured({"ROLE_ADMIN"})
    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Long id) {
        var optionalProduct = productService.findProduct(id);
        if (optionalProduct.isPresent()) {
            productService.removeProduct(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
