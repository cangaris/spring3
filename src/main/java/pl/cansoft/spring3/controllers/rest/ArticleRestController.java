package pl.cansoft.spring3.controllers.rest;

import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.cansoft.spring3.models.Article;
import pl.cansoft.spring3.models.ArticleCategory;
import pl.cansoft.spring3.services.articles.ArticleService;

@RequiredArgsConstructor
@RestController
class ArticleRestController {

    private final ArticleService service;

    @GetMapping("rest/articles")
    public List<Article> getArticles(@RequestParam(required = false, name = "m") Integer month,
                                     @RequestParam(required = false, name = "y") Integer year,
                                     @RequestParam(required = false, name = "c") ArticleCategory category) {
        return service.getArticlesByParams(month, year, category);
    }

    @GetMapping("rest/articles/{id}")
    public ResponseEntity<Article> getArticle(@PathVariable Long id) {
        var optionalArticle = service.findArticle(id);
        if (optionalArticle.isPresent()) {
            return ResponseEntity.ok(optionalArticle.get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER", "ROLE_AUTHOR"})
    @PostMapping("rest/articles")
    public ResponseEntity<Article> addArticle(@RequestBody @Valid Article article) {
        return ResponseEntity.ok(service.addArticle(article));
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER"})
    @PutMapping("rest/articles/{id}")
    public ResponseEntity<Article> editArticle(@PathVariable Long id, @RequestBody @Valid Article article) {
        return ResponseEntity.ok(service.updateArticle(id, article));
    }

    @Secured({"ROLE_ADMIN"})
    @DeleteMapping("rest/articles/{id}")
    public ResponseEntity<Void> removeArticle(@PathVariable Long id) {
        var optionalArticle = service.findArticle(id);
        if (optionalArticle.isPresent()) {
            service.removeArticle(id);
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.notFound().build();
        }
    }
}
