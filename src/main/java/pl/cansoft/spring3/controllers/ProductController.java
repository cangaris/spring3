package pl.cansoft.spring3.controllers;

import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.cansoft.spring3.models.Product;
import pl.cansoft.spring3.models.ProductCategory;
import pl.cansoft.spring3.services.products.ProductService;

@Slf4j
@RequiredArgsConstructor
@Controller
class ProductController {

    private final ProductService service;

    @GetMapping("products")
    public String products(
        @RequestParam(required = false, name = "pf") Integer priceFrom,
        @RequestParam(required = false, name = "pt") Integer priceTo,
        @RequestParam(required = false, name = "n") String name,
        @RequestParam(required = false, name = "c") ProductCategory category,
        Model model) {
        var filtered = service.getProductsByParams(priceFrom, priceTo, name, category);
        model.addAttribute("categories", ProductCategory.values());
        model.addAttribute("items", filtered);
        model.addAttribute("priceFrom", priceFrom);
        model.addAttribute("priceTo", priceTo);
        model.addAttribute("name", name);
        model.addAttribute("category", category);
        return "products/products";
    }

    @GetMapping("show-product/{id}")
    public String showProductDetails(@PathVariable Long id, Model model) {
        var optionalProduct = service.findProduct(id);
        if (optionalProduct.isPresent()) {
            var dbProduct = optionalProduct.get();
            model.addAttribute("product", dbProduct);
            return "products/product";
        } else {
            return "redirect:/products";
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER", "ROLE_AUTHOR"})
    @GetMapping("add-product")
    public String addProductForm(Model model) {
        model.addAttribute("categories", ProductCategory.values());
        model.addAttribute("header", "Add product");
        return "admin/saveProduct";
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER", "ROLE_AUTHOR"})
    @PostMapping("add-product")
    public String addProduct(@Valid Product product, BindingResult result, RedirectAttributes attr) {
        if (result.hasErrors()) {
            var errorMessage = result.getAllErrors().get(0).getDefaultMessage();
            log.error("Validation save error: {}", errorMessage);
            attr.addFlashAttribute("product", product);
            attr.addFlashAttribute("hasErrors", result.hasErrors());
            attr.addFlashAttribute("errorMessage", errorMessage);
            return "redirect:/add-product";
        }
        service.addProduct(product);
        return "redirect:/products";
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER"})
    @GetMapping("edit-product/{id}")
    public String showEditProductForm(@PathVariable Long id, Model model) {
        var optionalProduct = service.findProduct(id);
        if (optionalProduct.isPresent()) {
            var dbProduct = optionalProduct.get();
            model.addAttribute("categories", ProductCategory.values());
            model.addAttribute("product", dbProduct);
            model.addAttribute("header", "Edit product");
            return "admin/saveProduct";
        } else {
            return "redirect:/products";
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER"})
    @PostMapping("edit-product/{id}")
    public String editProduct(@PathVariable Long id, @Valid Product formProduct,
                              BindingResult result, RedirectAttributes attr) {
        if (result.hasErrors()) {
            var errorMessage = result.getAllErrors().get(0).getDefaultMessage();
            log.error("Validation save error: {}", errorMessage);
            attr.addFlashAttribute("hasErrors", result.hasErrors());
            attr.addFlashAttribute("errorMessage", errorMessage);
            return "redirect:/edit-product/" + id;
        }
        service.updateProduct(id, formProduct);
        return "redirect:/products";
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("remove-product/{id}")
    public String removeArticle(@PathVariable Long id) {
        service.removeProduct(id);
        return "redirect:/products";
    }
}
