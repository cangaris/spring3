package pl.cansoft.spring3.controllers;

import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import pl.cansoft.spring3.models.Article;
import pl.cansoft.spring3.models.ArticleCategory;
import pl.cansoft.spring3.services.articles.ArticleService;

@Slf4j
@RequiredArgsConstructor
@Controller
class ArticleController {

    private final ArticleService service;

    @GetMapping("blog")
    public String blog(@RequestParam(required = false, name = "m") Integer month,
                       @RequestParam(required = false, name = "y") Integer year,
                       @RequestParam(required = false, name = "c") ArticleCategory category,
                       Model model) {
        var filtered = service.getArticlesByParams(month, year, category);
        model.addAttribute("categories", ArticleCategory.values());
        model.addAttribute("category", category);
        model.addAttribute("items", filtered);
        model.addAttribute("month", month);
        model.addAttribute("year", year);
        return "articles/articles";
    }

    @GetMapping("show-article/{id}")
    public String showArticleDetails(@PathVariable Long id, Model model) {
        var optionalArticle = service.findArticle(id);
        if (optionalArticle.isPresent()) {
            var dbArticle = optionalArticle.get();
            model.addAttribute("article", dbArticle);
            return "articles/article";
        } else {
            return "redirect:/blog";
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER", "ROLE_AUTHOR"})
    @GetMapping("add-article")
    public String showAddArticleForm(Model model) {
        model.addAttribute("categories", ArticleCategory.values());
        model.addAttribute("header", "Add article");
        return "admin/saveArticle";
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER", "ROLE_AUTHOR"})
    @PostMapping("add-article")
    public String addArticle(@Valid Article article, BindingResult result, RedirectAttributes attr) {
        if (result.hasErrors()) {
            var errorMessage = result.getAllErrors().get(0).getDefaultMessage();
            log.error("Validation save error: {}", errorMessage);
            attr.addFlashAttribute("article", article);
            attr.addFlashAttribute("hasErrors", result.hasErrors());
            attr.addFlashAttribute("errorMessage", errorMessage);
            return "redirect:/add-article";
        }
        service.addArticle(article);
        return "redirect:/blog";
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER"})
    @GetMapping("edit-article/{id}")
    public String showEditArticleForm(@PathVariable Long id, Model model) {
        var optionalArticle = service.findArticle(id);
        if (optionalArticle.isPresent()) {
            var dbArticle = optionalArticle.get();
            model.addAttribute("article", dbArticle);
            model.addAttribute("categories", ArticleCategory.values());
            model.addAttribute("header", "Edit article");
            return "admin/saveArticle";
        } else {
            return "redirect:/blog";
        }
    }

    @Secured({"ROLE_ADMIN", "ROLE_MANAGER"})
    @PostMapping("edit-article/{id}")
    public String editArticle(@PathVariable Long id, @Valid Article formArticle,
                              BindingResult result, RedirectAttributes attr) {
        if (result.hasErrors()) {
            var errorMessage = result.getAllErrors().get(0).getDefaultMessage();
            log.error("Validation update error: {}", errorMessage);
            attr.addFlashAttribute("hasErrors", result.hasErrors());
            attr.addFlashAttribute("errorMessage", errorMessage);
            return "redirect:/edit-article/" + id;
        }
        service.updateArticle(id, formArticle);
        return "redirect:/blog";
    }

    @Secured("ROLE_ADMIN")
    @GetMapping("remove-article/{id}")
    public String removeArticle(@PathVariable Long id) {
        service.removeArticle(id);
        return "redirect:/blog";
    }
}
