package pl.cansoft.spring3.config;

import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import pl.cansoft.spring3.repositories.UserRepository;

@RequiredArgsConstructor
@Slf4j
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true, jsr250Enabled = true)
class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UserRepository userRepository;

    private static final String[] ALLOWED_URLS = {
        "/",
        "/js/**",
        "/css/**",
        "/about-us",
        "/contact",
        "/blog",
        "/products",
        "/show-article/{id}",
        "/show-product/{id}",
        "/login",
    };

    private static final String[] ADMIN_URLS = {
        // "/h2-console/**"
    };

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
            .cors().configurationSource(corsConfigurationSource());
        http
            .csrf().disable(); // wyłączenie zabezpieczeń csrf
        http
            .authorizeRequests()
            .antMatchers(ALLOWED_URLS).permitAll() // pozwala wszystkim zobaczyć zasób
            .antMatchers(ADMIN_URLS).hasRole("ADMIN") // urle tylko dla ADMINA
            .anyRequest().authenticated(); // reszta dla zalogowanych
        http
            .formLogin().loginPage("/login"); // formularz logowania
        http
            .rememberMe().key("JakieśTrudnyTajnyKod"); // generowanie tokenu odświerzenia sesji
        http
            .logout().logoutUrl("/logout"); // url wylogowania
        http
            .headers().frameOptions().sameOrigin(); // pozwolenie na wyświetlanie aplikacji w ramce
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth
            .userDetailsService(username -> {
                var optionalUser = userRepository.findByUsername(username);
                if (optionalUser.isEmpty()) {
                    throw new UsernameNotFoundException("Użytkownik nie istnieje");
                }
                var user = optionalUser.get();
                return User.builder()
                    .username(user.getUsername())
                    .password(user.getPassword())
                    .roles(user.getRole().name())
                    .build();
            })
            .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


    CorsConfigurationSource corsConfigurationSource() {
        var allowedMethods = List.of(
            HttpMethod.GET.name(),
            HttpMethod.POST.name(),
            HttpMethod.PUT.name(),
            HttpMethod.DELETE.name(),
            HttpMethod.OPTIONS.name()
        );
        var allowedUrls = List.of("http://localhost:4200");
        var configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(allowedUrls);
        configuration.setAllowedMethods(allowedMethods);
        configuration.addAllowedHeader("*");
        configuration.setAllowCredentials(true);
        var source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;
    }
}
