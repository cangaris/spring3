package pl.cansoft.spring3.services.products;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import pl.cansoft.spring3.models.Product;
import pl.cansoft.spring3.models.ProductCategory;

@Service
class ProductListService implements ProductService {

    private Long nextId;
    private List<Product> products = new ArrayList<>();

    public ProductListService() {
        var now = LocalDateTime.now();
        products.add(Product.builder().id(1L).price(100).name("iPhone X").content("Treść 1")
            .category(ProductCategory.PHONES).createdAt(now).image(
                "https://f01.esfr.pl/foto/1/30170930097/9a870ff915be052854943f128785106a/gear4-d3o-bank-iphone-7-8-plus-czarny,30170930097_8.jpg")
            .build());
        nextId = 2L;
    }

    @Override
    public Product addProduct(Product product) {
        product.setId(nextId);
        product.setCreatedAt(LocalDateTime.now());
        products.add(product);
        nextId++;
        return product;
    }

    @Override
    public void removeProduct(Long id) {
        products.removeIf(product -> id.equals(product.getId()));
    }

    @Override
    public Optional<Product> findProduct(Long id) {
        return products.stream()
            .filter(product -> product.getId().equals(id))
            .findFirst();
    }

    @Override
    public Product updateProduct(Long id, Product formProduct) {
        var optionalProduct = findProduct(id);
        if (optionalProduct.isPresent()) {
            var dbProduct = optionalProduct.get();
            dbProduct.setName(formProduct.getName());
            dbProduct.setContent(formProduct.getContent());
            dbProduct.setCategory(formProduct.getCategory());
            dbProduct.setPrice(formProduct.getPrice());
            dbProduct.setImage(formProduct.getImage());
            dbProduct.setUpdatedAt(LocalDateTime.now());
        }
        return formProduct;
    }

    @Override
    public List<Product> getProductsByParams(Integer priceFrom, Integer priceTo, String name,
                                             ProductCategory category) {
        return products.stream()
            .filter(product -> name == null || product.getName().contains(name))
            .filter(product -> category == null || product.getCategory().equals(category))
            .filter(product -> {
                if (product.getPrice() == null) {
                    return false;
                }
                var priceFromTemp = priceFrom;
                if (priceFromTemp == null) {
                    priceFromTemp = 0;
                }
                var priceToTemp = priceTo;
                if (priceToTemp == null) {
                    priceToTemp = Integer.MAX_VALUE;
                }
                return product.getPrice() >= priceFromTemp && product.getPrice() <= priceToTemp;
            })
            .toList();
    }
}
