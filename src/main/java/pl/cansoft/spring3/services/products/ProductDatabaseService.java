package pl.cansoft.spring3.services.products;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import pl.cansoft.spring3.exceptions.ItemNotFoundException;
import pl.cansoft.spring3.models.Product;
import pl.cansoft.spring3.models.ProductCategory;
import pl.cansoft.spring3.repositories.ProductRepository;

@RequiredArgsConstructor
@Primary
@Service
class ProductDatabaseService implements ProductService {

    private final ProductRepository repository;

    @Override
    public Product addProduct(Product product) {
        product.setCreatedAt(LocalDateTime.now());
        return repository.save(product);
    }

    @Override
    public void removeProduct(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Product> findProduct(Long id) {
        return repository.findById(id);
    }

    @Override
    public Product updateProduct(Long id, Product formProduct) {
        var optionalProduct = findProduct(id);
        if (optionalProduct.isPresent()) {
            var dbProduct = optionalProduct.get();
            dbProduct.setName(formProduct.getName());
            dbProduct.setContent(formProduct.getContent());
            dbProduct.setImage(formProduct.getImage());
            dbProduct.setPrice(formProduct.getPrice());
            dbProduct.setCategory(formProduct.getCategory());
            dbProduct.setUpdatedAt(LocalDateTime.now());
            return repository.save(dbProduct);
        }
        throw new ItemNotFoundException("Produkt nie odnaleziony");
    }

    @Override
    public List<Product> getProductsByParams(Integer priceFrom, Integer priceTo, String name,
                                             ProductCategory category) {
        return repository.findByParams(priceFrom, priceTo, name, category);
    }
}
