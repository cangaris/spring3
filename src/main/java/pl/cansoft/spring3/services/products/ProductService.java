package pl.cansoft.spring3.services.products;

import java.util.List;
import java.util.Optional;
import pl.cansoft.spring3.models.Product;
import pl.cansoft.spring3.models.ProductCategory;

public interface ProductService {

    Product addProduct(Product product);

    Product updateProduct(Long id, Product formProduct);

    void removeProduct(Long id);

    Optional<Product> findProduct(Long id);

    List<Product> getProductsByParams(Integer priceFrom, Integer priceTo, String name,
                                      ProductCategory category);
}
