package pl.cansoft.spring3.services.articles;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Service;
import pl.cansoft.spring3.models.Article;
import pl.cansoft.spring3.models.ArticleCategory;

@Service
class ArticleListService implements ArticleService {

    private Long nextId;
    private List<Article> articles = new ArrayList<>();

    public ArticleListService() {
        var now = LocalDateTime.now();
        articles.add(Article.builder().id(1L).title("Tytuł 1").content("Treść 1")
            .category(ArticleCategory.GUIDE).createdAt(now).build());
        articles.add(Article.builder().id(2L).title("Tytuł 2").content("Treść 2")
            .category(ArticleCategory.TECHNICAL).createdAt(now.minusMonths(2)).build());
        nextId = 3L;
    }

    @Override
    public Article addArticle(Article article) {
        article.setId(nextId);
        article.setCreatedAt(LocalDateTime.now());
        articles.add(article);
        nextId++;
        return article;
    }

    @Override
    public void removeArticle(Long id) {
        articles.removeIf(article -> id.equals(article.getId()));
    }

    @Override
    public Optional<Article> findArticle(Long id) {
        return articles.stream()
            .filter(article -> article.getId().equals(id))
            .findFirst();
    }

    @Override
    public Article updateArticle(Long id, Article formArticle) {
        var optionalArticle = findArticle(id);
        if (optionalArticle.isPresent()) {
            var dbArticle = optionalArticle.get();
            dbArticle.setTitle(formArticle.getTitle());
            dbArticle.setContent(formArticle.getContent());
            dbArticle.setCategory(formArticle.getCategory());
            dbArticle.setUpdatedAt(LocalDateTime.now());
            return dbArticle;
        }
        return null;
    }

    @Override
    public List<Article> getArticlesByParams(Integer month, Integer year, ArticleCategory category) {
        return articles.stream()
            .filter(article -> category == null || article.getCategory().equals(category))
            .filter(article -> month == null || article.getCreatedAt().getMonthValue() == month)
            .filter(article -> year == null || article.getCreatedAt().getYear() == year)
            .toList();
    }
}
