package pl.cansoft.spring3.services.articles;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import pl.cansoft.spring3.exceptions.ItemNotFoundException;
import pl.cansoft.spring3.models.Article;
import pl.cansoft.spring3.models.ArticleCategory;
import pl.cansoft.spring3.repositories.ArticleRepository;

@Slf4j
@RequiredArgsConstructor
@Primary
@Service
class ArticleDatabaseService implements ArticleService {

    private final ArticleRepository repository;

    @Override
    public Article addArticle(Article article) {
        article.setCreatedAt(LocalDateTime.now());
        log.info("Article to save: {}", article);
        return repository.save(article);
    }

    @Override
    public Article updateArticle(Long id, Article formArticle) {
        var optionalArticle = findArticle(id);
        if (optionalArticle.isPresent()) {
            var dbArticle = optionalArticle.get();
            dbArticle.setTitle(formArticle.getTitle());
            dbArticle.setContent(formArticle.getContent());
            dbArticle.setCategory(formArticle.getCategory());
            dbArticle.setUpdatedAt(LocalDateTime.now());
            return repository.save(dbArticle);
        }
        throw new ItemNotFoundException("Artykuł nie odnaleziony");
    }

    @Override
    public void removeArticle(Long id) {
        repository.deleteById(id);
    }

    @Override
    public Optional<Article> findArticle(Long id) {
        return repository.findById(id);
    }

    @Override
    public List<Article> getArticlesByParams(Integer month, Integer year, ArticleCategory category) {
        LocalDateTime from = null;
        LocalDateTime to = null;
        if (year != null && month != null) {
            from = LocalDate.of(year, month, 1).atStartOfDay();
            to = LocalDate.of(year, month + 1, 1).atStartOfDay();
        } else if (year == null && month != null) {
            from = LocalDate.of(LocalDate.now().getYear(), month, 1).atStartOfDay();
            to = LocalDate.of(LocalDate.now().getYear(), month + 1, 1).atStartOfDay();
        } else if (year != null) {
            from = LocalDate.of(year, 1, 1).atStartOfDay();
            to = LocalDate.of(year + 1, 1, 1).atStartOfDay();
        }
        return repository.findByParams(category, from, to);
    }
}
