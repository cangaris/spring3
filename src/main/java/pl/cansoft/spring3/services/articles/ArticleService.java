package pl.cansoft.spring3.services.articles;

import java.util.List;
import java.util.Optional;
import pl.cansoft.spring3.models.Article;
import pl.cansoft.spring3.models.ArticleCategory;

public interface ArticleService {

    Article addArticle(Article article);

    Article updateArticle(Long id, Article formArticle);

    void removeArticle(Long id);

    Optional<Article> findArticle(Long id);

    List<Article> getArticlesByParams(Integer month, Integer year, ArticleCategory category);
}
