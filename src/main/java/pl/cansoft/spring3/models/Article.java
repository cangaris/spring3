package pl.cansoft.spring3.models;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Article {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Tytuł jest wymagany")
    @Size(min = 5, max = 70, message = "Tytuł wymaga od 5-70 znaków")
    @Column(length = 70)
    private String title;

    @NotBlank(message = "Treść jest wymagana")
    @Size(min = 5, max = 1023, message = "Treść wymaga od 5-1023 znaków")
    @Column(length = 1023)
    private String content;

    @NotNull(message = "Kategoria jest wymagana")
    @Enumerated(EnumType.STRING)
    private ArticleCategory category;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
