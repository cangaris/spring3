package pl.cansoft.spring3.models;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank(message = "Nazwa produktu jest wymagana")
    @Size(min = 5, max = 70, message = "Nazwa produktu wymaga od 5-70 znaków")
    @Column(length = 70)
    private String name;

    @NotBlank(message = "Link do obrazka jest wymagany")
    @Size(min = 5, max = 255, message = "Link do obrazka wymaga od 5-255 znaków")
    private String image;

    @NotBlank(message = "Treść produktu jest wymagana")
    @Size(min = 5, max = 1023, message = "Treść produktu wymaga od 5-1023 znaków")
    @Column(length = 1023)
    private String content;

    @NotNull(message = "Cena jest wymagana")
    @Min(value = 1, message = "Cena minimalna to 1")
    @Max(value = 100_000, message = "Cena maksymalna to 100 000")
    private Integer price;

    @NotNull(message = "Kategoria jest wymagana")
    @Enumerated(EnumType.STRING)
    private ProductCategory category;

    private LocalDateTime createdAt;

    private LocalDateTime updatedAt;
}
