package pl.cansoft.spring3.integrations;

import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ArticleRestControllerTest {

    @Autowired
    MockMvc mockMvc;

    @WithMockUser(roles = "AUTHOR")
    @Test
    @Order(1)
    void testAddArticle() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.post("/rest/articles")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {
                        "title": "rewrewrew",
                        "content": "fsdfdsfds",
                        "category": "TECHNICAL"
                    }
                    """))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(
                """
                    {
                        "id": 1,
                        "title": "rewrewrew",
                        "content": "fsdfdsfds",
                        "category": "TECHNICAL"
                    }
                    """
            ));
    }

    @WithMockUser
    @Test
    @Order(2)
    void testGetArticles() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/rest/articles"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(
                """
                    [
                        {
                            "id": 1,
                            "title": "rewrewrew",
                            "content": "fsdfdsfds",
                            "category": "TECHNICAL"
                        }
                    ]
                    """
            ));
    }

    @WithMockUser(roles = "MANAGER")
    @Test
    @Order(3)
    void testEditArticle() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.put("/rest/articles/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {
                        "title": "rewrewrew",
                        "content": "fsdfdsfds",
                        "category": "GUIDE"
                    }
                    """))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(
                """
                    {
                        "id": 1,
                        "title": "rewrewrew",
                        "content": "fsdfdsfds",
                        "category": "GUIDE"
                    }
                    """
            ));
    }

    @WithMockUser
    @Test
    @Order(4)
    void testGetArticleById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/rest/articles/1"))
            .andExpect(MockMvcResultMatchers.status().isOk())
            .andExpect(MockMvcResultMatchers.content().json(
                """
                    {
                        "id": 1,
                        "title": "rewrewrew",
                        "content": "fsdfdsfds",
                        "category": "GUIDE"
                    }
                    """
            ));
    }

    @WithMockUser(roles = "ADMIN")
    @Test
    @Order(5)
    void testDeleteArticleById() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/rest/articles/1"))
            .andExpect(MockMvcResultMatchers.status().isNoContent());
    }

    @WithMockUser
    @Test
    @Order(6)
    void testGetArticleByIdAfterDelete() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/rest/articles/1"))
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @WithMockUser(roles = "ADMIN")
    @Test
    @Order(7)
    void testDeleteArticleByIdForNotFoundItem() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.delete("/rest/articles/1"))
            .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
