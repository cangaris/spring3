package pl.cansoft.spring3.services.articles;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.cansoft.spring3.exceptions.ItemNotFoundException;
import pl.cansoft.spring3.models.Article;
import pl.cansoft.spring3.models.ArticleCategory;
import pl.cansoft.spring3.repositories.ArticleRepository;

@ExtendWith(MockitoExtension.class)
class ArticleDatabaseServiceTest {

    @InjectMocks
    ArticleDatabaseService service;

    @Mock
    ArticleRepository repository;

    @Test
    void shouldCallRepositorySaveFnAndHaveCreatedAtAndIdNotNull() {
        // given
        var articleToSave = Article.builder().build();
        var savedArticle = Article.builder()
            .id(1L)
            .createdAt(LocalDateTime.now())
            .build();
        // when
        Mockito.when(repository.save(Mockito.any(Article.class)))
            .thenReturn(savedArticle);
        var response = service.addArticle(articleToSave);
        // then
        Mockito.verify(repository).save(Mockito.any(Article.class));
        Assertions.assertThat(response.getCreatedAt()).isNotNull();
        Assertions.assertThat(response.getId()).isNotNull();
    }

    @Test
    void shouldCallRepositoryUpdateFnWhenFoundArticleInRepository() {
        // given
        var articleId = 1L;
        var articleToUpdate = Article.builder()
            .id(articleId)
            .content("test content")
            .title("test title")
            .category(ArticleCategory.TECHNICAL)
            .createdAt(LocalDateTime.now())
            .build();
        var articleUpdated = Article.builder()
            .id(articleId)
            .content("test content")
            .title("test title")
            .category(ArticleCategory.TECHNICAL)
            .createdAt(LocalDateTime.now())
            .updatedAt(LocalDateTime.now())
            .build();
        // when
        Mockito.when(repository.findById(Mockito.anyLong()))
            .thenReturn(Optional.of(articleToUpdate));
        Mockito.when(repository.save(Mockito.any(Article.class)))
            .thenReturn(articleUpdated);
        var response = service.updateArticle(articleId, articleToUpdate);
        // then
        Mockito.verify(repository).save(Mockito.any(Article.class));
        Mockito.verify(repository).findById(Mockito.anyLong());
        Assertions.assertThat(response.getCreatedAt()).isNotNull();
        Assertions.assertThat(response.getUpdatedAt()).isNotNull();
        Assertions.assertThat(response.getId()).isNotNull();
        Assertions.assertThat(response.getCategory()).isEqualTo(articleToUpdate.getCategory());
        Assertions.assertThat(response.getTitle()).isEqualTo(articleToUpdate.getTitle());
        Assertions.assertThat(response.getContent()).isEqualTo(articleToUpdate.getContent());
        Assertions.assertThat(response.getId()).isEqualTo(articleToUpdate.getId());
    }

    @Test
    void shouldThrowExceptionWhenCouldNotFindArticleInRepository() {
        // given
        var articleId = 1L;
        var articleToUpdate = Article.builder().build();
        // when
        Mockito.when(repository.findById(Mockito.anyLong()))
            .thenReturn(Optional.empty());
        // then
        Assertions.assertThatThrownBy(() -> service.updateArticle(articleId, articleToUpdate))
            .isInstanceOf(ItemNotFoundException.class);
        Mockito.verify(repository).findById(Mockito.anyLong());
    }

    @Test
    void shouldCallRepositoryDeleteByIdFn() {
        // when
        service.removeArticle(1L);
        // then
        Mockito.verify(repository).deleteById(Mockito.anyLong());
    }

    @Test
    void shouldCallRepositoryFindByIdFn() {
        // when
        service.findArticle(1L);
        // then
        Mockito.verify(repository).findById(Mockito.anyLong());
    }

    @Test
    void shouldCallRepositoryFindByParamsForNotNullYearAndNotNullMonth() {
        // given
        var articleId = 1L;
        var articles = List.of(Article.builder().id(articleId).build());
        var month = 1;
        var year = 2022;
        var category = ArticleCategory.TECHNICAL;
        var from = LocalDate.of(year, month, 1).atStartOfDay();
        var to = LocalDate.of(year, month + 1, 1).atStartOfDay();
        // when
        Mockito.when(repository.findByParams(category, from, to)).thenReturn(articles);
        var response = service.getArticlesByParams(month, year, category);
        // then
        Mockito.verify(repository).findByParams(category, from, to);
        Assertions.assertThat(response).hasSize(1);
        Assertions.assertThat(response.get(0).getId()).isEqualTo(articleId);
    }

    @Test
    void shouldCallRepositoryFindByParamsForNullYearAndNotNullMonth() {
        // given
        var articleId = 1L;
        var articles = List.of(Article.builder().id(articleId).build());
        var month = 1;
        Integer year = null;
        var category = ArticleCategory.TECHNICAL;
        var from = LocalDate.of(LocalDate.now().getYear(), month, 1).atStartOfDay();
        var to = LocalDate.of(LocalDate.now().getYear(), month + 1, 1).atStartOfDay();
        // when
        Mockito.when(repository.findByParams(category, from, to)).thenReturn(articles);
        var response = service.getArticlesByParams(month, year, category);
        // then
        Mockito.verify(repository).findByParams(category, from, to);
        Assertions.assertThat(response).hasSize(1);
        Assertions.assertThat(response.get(0).getId()).isEqualTo(articleId);
    }

    @Test
    void shouldCallRepositoryFindByParamsForNotNullYear() {
        // given
        var articleId = 1L;
        var articles = List.of(Article.builder().id(articleId).build());
        Integer month = null;
        var year = 2022;
        var category = ArticleCategory.TECHNICAL;
        var from = LocalDate.of(year, 1, 1).atStartOfDay();
        var to = LocalDate.of(year + 1, 1, 1).atStartOfDay();
        // when
        Mockito.when(repository.findByParams(category, from, to)).thenReturn(articles);
        var response = service.getArticlesByParams(month, year, category);
        // then
        Mockito.verify(repository).findByParams(category, from, to);
        Assertions.assertThat(response).hasSize(1);
        Assertions.assertThat(response.get(0).getId()).isEqualTo(articleId);
    }
}
