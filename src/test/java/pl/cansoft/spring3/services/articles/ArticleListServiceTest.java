package pl.cansoft.spring3.services.articles;

import java.time.LocalDateTime;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.cansoft.spring3.models.Article;
import pl.cansoft.spring3.models.ArticleCategory;

@ExtendWith(MockitoExtension.class)
class ArticleListServiceTest {

    @InjectMocks
    ArticleListService service;

    @Test
    @DisplayName("should has size 3 after add article")
    void shouldHasSize3AfterAddArticle() {
        // given
        var article = Article.builder().build();
        // when
        service.addArticle(article);
        // then
        var list = service.getArticlesByParams(null, null, null);
        Assertions.assertThat(list).hasSize(3);
        Assertions.assertThat(list.get(2).getId()).isEqualTo(3);
        Assertions.assertThat(list.get(2).getCreatedAt()).isNotNull();
    }

    @Test
    void shouldHasSize1AfterRemoveArticle() {
        // when
        service.removeArticle(1L);
        // then
        var list = service.getArticlesByParams(null, null, null);
        Assertions.assertThat(list).hasSize(1);
        Assertions.assertThat(list.get(0).getId()).isEqualTo(2);
    }

    @Test
    void shouldFindArticleForExistingId() {
        // given
        var articleId = 1L;
        // when
        var optionalArticle = service.findArticle(articleId);
        // then
        Assertions.assertThat(optionalArticle.isPresent()).isTrue();
        Assertions.assertThat(optionalArticle.get().getId()).isEqualTo(articleId);
    }

    @Test
    void shouldNotFindArticleForNotExistingId() {
        // given
        var articleId = 3L;
        // when
        var optionalArticle = service.findArticle(articleId);
        // then
        Assertions.assertThat(optionalArticle.isEmpty()).isTrue();
    }

    @Test
    void shouldUpdateArticleForExistingId() {
        // given
        var articleId = 1L;
        var article = Article.builder()
            .id(articleId)
            .title("different title")
            .content("different content")
            .category(ArticleCategory.TECHNICAL)
            .updatedAt(LocalDateTime.now())
            .build();
        // when
        var result = service.updateArticle(articleId, article);
        // then
        Assertions.assertThat(result.getId()).isEqualTo(article.getId());
        Assertions.assertThat(result.getTitle()).isEqualTo(article.getTitle());
        Assertions.assertThat(result.getContent()).isEqualTo(article.getContent());
        Assertions.assertThat(result.getCategory()).isEqualTo(article.getCategory());
        Assertions.assertThat(result.getUpdatedAt()).isNotNull();
    }

    @Test
    void shouldNotUpdateArticleForNotExistingId() {
        // given
        var articleId = 3L;
        var article = Article.builder()
            .id(articleId)
            .title("different title")
            .content("different content")
            .category(ArticleCategory.TECHNICAL)
            .updatedAt(LocalDateTime.now())
            .build();
        // when
        var result = service.updateArticle(articleId, article);
        // then
        Assertions.assertThat(result).isNull();
    }

    @Test
    void shouldReturnFullUnfilteredList() {
        // when
        var list = service.getArticlesByParams(null, null, null);
        // then
        Assertions.assertThat(list).hasSize(2);
    }

    @Test
    void shouldReturnFilteredListByCategory() {
        // when
        var list = service.getArticlesByParams(null, null, ArticleCategory.TECHNICAL);
        // then
        Assertions.assertThat(list).hasSize(1);
        Assertions.assertThat(list.get(0).getCategory()).isEqualTo(ArticleCategory.TECHNICAL);
    }

    @Test
    void shouldReturnFilteredListByYear() {
        // when
        var year = LocalDateTime.now().getYear();
        var list = service.getArticlesByParams(null, year, null);
        // then
        if (list.size() == 1) {
            Assertions.assertThat(list).hasSize(1);
            Assertions.assertThat(list.get(0).getCategory()).isEqualTo(ArticleCategory.GUIDE);
        } else if (list.size() == 2) {
            Assertions.assertThat(list).hasSize(2);
            Assertions.assertThat(list.get(0).getCategory()).isEqualTo(ArticleCategory.GUIDE);
            Assertions.assertThat(list.get(1).getCategory()).isEqualTo(ArticleCategory.TECHNICAL);
        } else {
            throw new IllegalStateException("list cannot be empty");
        }
    }

    @Test
    void shouldReturnFilteredListByMonth() {
        // when
        var month = LocalDateTime.now().getMonthValue();
        var list = service.getArticlesByParams(month, null, null);
        // then
        Assertions.assertThat(list).hasSize(1);
        Assertions.assertThat(list.get(0).getCategory()).isEqualTo(ArticleCategory.GUIDE);
    }

    @Test
    void shouldReturnFilteredListByMonthYearAndCategory() {
        // when
        var month = LocalDateTime.now().getMonthValue();
        var year = LocalDateTime.now().getYear();
        var list = service.getArticlesByParams(month, year, ArticleCategory.GUIDE);
        // then
        Assertions.assertThat(list).hasSize(1);
        Assertions.assertThat(list.get(0).getCategory()).isEqualTo(ArticleCategory.GUIDE);
        Assertions.assertThat(list.get(0).getId()).isEqualTo(1);
    }
}
