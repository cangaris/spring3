#
# SQL Export
# Created by Querious (201069)
# Created: 5 June 2022 at 15:33:16 CEST
# Encoding: Unicode (UTF-8)
#


SET @PREVIOUS_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS;
SET FOREIGN_KEY_CHECKS = 0;


LOCK TABLES `article` WRITE;
TRUNCATE `article`;
ALTER TABLE `article`
    DISABLE KEYS;
REPLACE INTO `article` (`id`, `category`, `content`, `created_at`, `title`, `updated_at`)
VALUES (1, 'TECHNICAL', 'fsdfdsfds', '2022-06-05 15:25:00.000000', 'rewrewrew', NULL);
ALTER TABLE `article`
    ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `user` WRITE;
TRUNCATE `user`;
ALTER TABLE `user`
    DISABLE KEYS;
REPLACE INTO `user` (`id`, `password`, `role`, `username`, `phone_id`)
VALUES (1, '$2a$10$cC3q4HLOIYRufa0z/32hCufwW83diTEjdaVzNDQLChbzcJgbT0lP6', 'ADMIN', 'admin', 1);
ALTER TABLE `user`
    ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `user_address_pivot` WRITE;
TRUNCATE `user_address_pivot`;
ALTER TABLE `user_address_pivot`
    DISABLE KEYS;
REPLACE INTO `user_address_pivot` (`user_id`, `address_id`)
VALUES (1, 1);
ALTER TABLE `user_address_pivot`
    ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `product` WRITE;
TRUNCATE `product`;
ALTER TABLE `product`
    DISABLE KEYS;
REPLACE INTO `product` (`id`, `category`, `content`, `created_at`, `image`, `name`, `price`, `updated_at`)
VALUES (1, 'PHONES', 'sdfdsfdsf', '2022-06-05 15:26:26.000000',
        'https://www.imperiumtapet.com/public/uploads/preview/anonymus-39821533975987gxbmjiwbfn.jpg', 'fsdfds', 5555,
        NULL);
ALTER TABLE `product`
    ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `user_address` WRITE;
TRUNCATE `user_address`;
ALTER TABLE `user_address`
    DISABLE KEYS;
REPLACE INTO `user_address` (`id`, `city`, `street`)
VALUES (1, '00-000 Warszawa', 'Nowogrodzka 88/6');
ALTER TABLE `user_address`
    ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `user_email` WRITE;
TRUNCATE `user_email`;
ALTER TABLE `user_email`
    DISABLE KEYS;
REPLACE INTO `user_email` (`id`, `email`, `user_id`)
VALUES (1, 'd@cd.pl', 1);
ALTER TABLE `user_email`
    ENABLE KEYS;
UNLOCK TABLES;


LOCK TABLES `user_phone` WRITE;
TRUNCATE `user_phone`;
ALTER TABLE `user_phone`
    DISABLE KEYS;
REPLACE INTO `user_phone` (`id`, `phone`)
VALUES (1, '4444');
ALTER TABLE `user_phone`
    ENABLE KEYS;
UNLOCK TABLES;



SET FOREIGN_KEY_CHECKS = @PREVIOUS_FOREIGN_KEY_CHECKS;


